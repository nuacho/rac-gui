######################################################
#                 1C Rac GUI
# Distributed under GNU Public License
# Author: Sergey Kalinin svk@nuk-svk.ru
# http://nuk-svk.ru
# Home page: https://bitbucket.org/svk28/rac-gui
######################################################

"Rac GUI" - это графическая оболочка для утилиты управления кластером серверов 1С.
Для работы требуется Tcl/Tk не ниже 8.6 версии и пакет 1c-server куда входит утилита rac.
Позволяет работать с несколькими 1С-платформами одновременно.

http://nuk-svk.ru/soft/rac-gui/
https://git.nuk-svk.ru/svk/rac-gui.git

Иконки взяты с сайта https://www.iconsdb.com/royal-blue-icons/



