Name:           rac-gui
Version:        1.0.2
Release: 1
Summary:        1C rac gui tool
License:        GPL
Group:          System
Url:            https://bitbucket.org/svk28/rac-gui
BuildArch:      noarch
Source:         %name-%version-%release.tar.gz
Requires:       tcl => 8.6.8, tk >= 8.6.8

%description
This program ia a grafics user interface for 1C rac util.

%description -l ru_RU.UTF8
Графический интерфейс к утилите управления кластером серверов 1С rac

%prep
%setup -n %name

%build

%install
mkdir -p $RPM_BUILD_ROOT{%_bindir,%_datadir/%name/lib/msg}
install -p -m755 rac_gui.tcl $RPM_BUILD_ROOT%_bindir/racgui
%{__sed} -i 's+^set\ dir(lib)+set\ dir(lib)\ %_datadir/%name/lib ;#+g' $RPM_BUILD_ROOT%_bindir/racgui
%{__sed} -i 's+\[pwd\]+%_datadir/%name+g' $RPM_BUILD_ROOT%_bindir/racgui
%{__sed} -i 's+^set\ dir(doc)+set\ dir(doc)\ %_docdir/%name ;#+g' $RPM_BUILD_ROOT%_bindir/racgui
install -p -m644 lib/*.tcl $RPM_BUILD_ROOT%_datadir/%name/lib
install -p -m644 rac_gui.cfg $RPM_BUILD_ROOT%_datadir/%name/
install -p -m644 lib/msg/*.* $RPM_BUILD_ROOT%_datadir/%name/lib/msg/

# Menu support
#mkdir -p $RPM_BUILD_ROOT/usr/lib/menu
#cat > $RPM_BUILD_ROOT%_libdir/menu/%name << EOF
#?package(%name): needs=x11 icon="projman.png" section="Applications/Development/Development environments"  title=ProjMan longtitle="Tcl/Tk Project Manager" command=projman
#EOF
#mdk icons
#install -d $RPM_BUILD_ROOT{%_iconsdir,%_liconsdir,%_miconsdir}
#install -p -m644 img/icons/%name.png $RPM_BUILD_ROOT%_iconsdir/
#install -p -m644 img/icons/large/%name.png $RPM_BUILD_ROOT%_liconsdir/
#install -p -m644 img/icons/mini/%name.png $RPM_BUILD_ROOT%_miconsdir/

%post
%update_menus

%postun
%clean_menus

%files
%doc README.md
%doc doc/*
%doc debian/*
%_bindir/racgui
%_datadir/%name
#%_libdir/menu/%name
#%_iconsdir/%name.png
#%_liconsdir/%name.png
#%_miconsdir/%name.png


%changelog
* Wed Sep 25 2019 Sergey Kalinin <svk@nuk-svk.ru> 1.1.0
- Added new rac command options
- Added HELP dialog
- Moved configuration files according to XDG standard
- Change 1c-srv.cfg format
- Fixed some errors
- Added translated method for table headers

* Thu Aug 15 2019 Sergey Kalinin <svk@nuk-svk.ru> 1.0.3
- New server config
- Changes rac_gui on rac-gui 

* Wed Jul 24 2019 Sergey Kalinin <svk@nuk-svk.ru> 1.0.2
- CI/CD integration

* Mon Aug 06 2018 Sergey Kalinin <svk@nuk-svk.ru> 1.0.0
- Initial release

