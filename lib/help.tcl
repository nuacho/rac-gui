#######################################################
#                RAC GUI
#        Distributed under GNU Public License
# Author: Sergey Kalinin svk@nuk-svk.ru
# Copyright (c) "SVK", 2019, http://nuk-svk.ru
######################################################

set help_text {
    Программа Rac-GUI представляет собой мультиплатформенный графический интерфейс к консольной утилите 1С rac.
    Основной интерфейс программы состоит из:
    1. Панели инструментов
    2. Дерева для отображения структуры
    3. Табличного поля
    
    Для добавления, редактирования, удаления сущностей (сервера, информационные базы, сеансы) предназначены соответствующие кнопки, расположенные в панели инструментов слева. Кнопки контекстно-зависимые, т.е. в зависимости от того какой раздел активен для работы (сервер, информационные базы, кластер, сеансы и так далее), функции кнопок меняются.
    
    После первого запуска программы следует добавить основной сервер кластера 1С (где запущен ras). Для этого надо нажать кнопку "+" в панели инструментов, или сочетание клавиш Control-Insert. И в открывшейся форме заполнить соответствующие поля. Так как каждый сервер управления и клиент могут работать только на одинаковой платформе 1С, то в RAC-GUI для каждого сервера указывается команда (файл rac) для соответствующей платформы.
    
    = "Горячие" клавиши =
    
    Insert - добавление нового
    Delete - удаление текущего раздела (сущности)
    Control-Enter -  редактирование текущего раздела (сущности)
    Control-Q - выход из программы
    F1 - вызов дилога помощи
    
    = Настройки серверов =
    При первом старте, после добавления сервера и щелчка на нем мышкой, список кластеров, серверов и ИБ будет автоматически сохранён (при выходе) в файл настроек (конфигурации)
    
    = Формат файла конфигурации =
    Формат файла настроект серверов похож на JSON, но представляет собой несколько TCL-словарей вложенных друг в друга.
    Обязательными значениями являются адрес хоста, номер порта и путь к RAC. Остальные настройки опциональны.
    Опции сервера:
    name - наименование сервера
    rac_cmd - полный путь к утилите rac
    agent_user - имя администратора агента кластера
    agent_pwd  - пароль администратора агента кластера
    
    Опции кластера:
    cluster_name - наименование кластера
    cluster_user - имя администратора кластера
    cluster_pwd - пароль администратора кластера
    
    Опции инф. базы:
    infobase_name - наименование ИБ
    infobase_user - имя пользователя ИБ
    infobase_pwd - пароль пользователя ИБ
    
    Адреса хостов, номера портов, идентификаторы кластеров и ИБ должны соответствовать реальным.
    
    Пример настроек:
    
    localhost:1545 {
        name "Локальный сервер"
        rac_cmd "/opt/1C/v8.3/x86_64/rac"
        agent_user ""
        agent_pwd ""
        clusters {
            3ed9081a-c5b0-11e9-cf8a-1c1b0d94027e {
                cluster_name "eeee"
            }
            4581a966-a6bf-11e9-3c95-1c1b0d94027e {
                cluster_name "Локальный кластер"
                cluster_user ""
                cluster_pwd ""
                infobases {
                    1e1d1cea-b856-11e9-748a-1c1b0d94027e {
                        infobase_name "wewe"
                        infobase_user "ibuser"
                        infobase_pwd "ibpwd"
                    }
                    0129b2b2-b8d6-11e9-748a-1c1b0d94027e {
                        infobase_name "testdb"
                        infobase_user "testdb2-user"
                        infobase_pwd "testdb2-pwd"
                    }
                    cac80302-b855-11e9-748a-1c1b0d94027e {
                        infobase_name "testdb"
                    }
                }
            }
        }
    }
    192.168.125.145:1545 {
        name "Test"
        rac_cmd /opt/1C/v8.3/x86_64/rac"
        clusters {
        }
    }
    1c-srv:1545 {
        name "Cервер для бухгалтерии"
        rac_cmd "/opt/1C/v8.3.13_1644/x86_64/rac"
        clusters {
        }
    }
}


proc ShowHelpDialog {} {
    global default dir racgui_version racgui_release help_text
    set frm [AddToplevel [::msgcat::mc "About"] help_grey_64]
    wm title .add [::msgcat::mc "About"]
    ttk::label $frm.lbl_version -text "[::msgcat::mc "Version"]: $racgui_version"
    ttk::label $frm.lbl_release -text "[::msgcat::mc "Release"]: $racgui_release"
    
    text $frm.txt -wrap word
    ttk::scrollbar $frm.hsb -orient horizontal -command [list $frm.t xview]
    ttk::scrollbar $frm.vsb -orient vertical -command [list $frm.t yview]
    text $frm.t -xscrollcommand [list $frm.hsb set] -yscrollcommand [list $frm.vsb set]
    grid $frm.lbl_version -row 0 -column 0 -sticky nsew -padx 5 -pady 2
    grid $frm.lbl_release -row 1 -column 0 -sticky nsew  -padx 5 -pady 2
    grid $frm.t -row 2 -column 0 -sticky nsew
    grid $frm.vsb -row 2 -column 1 -sticky nsew
    grid $frm.hsb -row 3 -column 0 -sticky nsew
    grid columnconfigure $frm 0 -weight 1
    grid rowconfigure $frm 0 -weight 1
    
    $frm.t insert end $help_text
    destroy .add.frm_btn.btn_ok
    return $frm    
}



