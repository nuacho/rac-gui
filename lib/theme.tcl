######################################################
#        Rac GUI
# Graphical theme settings
#
# Distributed under GNU Public License
# Author: Sergey Kalinin svk@nuk-svk.ru
# Home page: https://nuk-svk.ru
# Git repos: https://bitbucket.org/svk28/rac-gui


######################################################

# set editor(fg) grey
# set editor(bg) black
# option add *Entry.Foreground $editor(fg) interactive
# option add *Entry.Background $editor(bg) interactive
# option add *Label.Foreground $editor(fg) interactive
# option add *Label.Background $editor(bg) interactive
# option add *Checkbox.Foreground $editor(fg) interactive
# option add *Checkbox.Background $editor(bg) interactive
# option add *Checkbutton.Foreground $editor(fg) interactive
# option add *Checkbutton.Background $editor(bg) interactive
# option add *Combobox.Foreground $editor(fg) interactive
# option add *Combobox.Background $editor(bg) interactive
# option add *Listbox.Foreground $editor(fg) interactive
# option add *Listbox.Background $editor(bg) interactive
# option add *TreeView.Background $editor(bg) interactive
#option add *Treeview.Foreground red interactive
# #option add *Frame.Background $editor(bg) interactive
# option add *ScrollableFrame.Background $editor(bg) interactive
# option add *ScrolledWindow.Background $editor(bg) interactive


if {[info exists default(theme)] == 1  && $default(theme) eq "dark"} {
    set color(dark_table_bg) "#6f6f6f"
    set color(light_table_bg) "#424242"
} else {
    set color(dark_table_bg) "#e2e2e2"
    set color(light_table_bg) "#ffffff"
}
