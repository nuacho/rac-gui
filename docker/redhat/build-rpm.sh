#!/bin/bash


git clone https://bitbucket.org/svk28/rac-gui.git
WORKDIR=/home/rpmbuild

VERSION=$(grep Version ${WORKDIR}/rac-gui/rac_gui.tcl | grep -oE '\b[0-9]{1,2}\.[0-9]{1,2}\.[0-9]{1,2}\b')
RELEASE=$(grep Release ${WORKDIR}/rac-gui/rac_gui.tcl | grep -oE '\b[0-9]{1,3}\b')

tar czf ${WORKDIR}/rpmbuild/SOURCES/rac-gui-${VERSION}-${RELEASE}.tar.gz rac-gui
cp ${WORKDIR}/rac-gui/rac_gui.spec ${WORKDIR}/rpmbuild/SPECS/rac_gui.spec

sed -i "s/.*Version:.*/Version:\t${VERSION}/" ${WORKDIR}/rpmbuild/SPECS/rac_gui.spec
sed -i "s/.*Release:.*/Release:\t${RELEASE}/" ${WORKDIR}/rpmbuild/SPECS/rac_gui.spec

rpmbuild -ba ${WORKDIR}/rpmbuild/SPECS/rac_gui.spec

#exit

cp ${WORKDIR}/rpmbuild/RPMS/noarch/rac-gui-${VERSION}-${RELEASE}.noarch.rpm /files/
cp ${WORKDIR}/rpmbuild/SRPMS/rac-gui-${VERSION}-${RELEASE}.src.rpm /files/
